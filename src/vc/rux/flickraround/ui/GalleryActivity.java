package vc.rux.flickraround.ui;

import android.app.Activity;
import android.location.*;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Toast;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.googlecode.androidannotations.annotations.*;
import com.googlecode.flickrjandroid.Flickr;
import com.googlecode.flickrjandroid.photos.Photo;
import com.googlecode.flickrjandroid.photos.PhotoList;
import com.googlecode.flickrjandroid.photos.PhotosInterface;
import com.googlecode.flickrjandroid.photos.SearchParameters;
import vc.rux.flickraround.Constants;
import vc.rux.flickraround.R;
import vc.rux.flickraround.ui.components.GalleryAdapter;
import vc.rux.flickraround.ui.fragments.PhotoFragment;
import vc.rux.flickraround.ui.fragments.PhotoFragment_;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;


@EActivity(R.layout.main)
@OptionsMenu(R.menu.main)
public class GalleryActivity extends SherlockFragmentActivity {
    @ViewById(R.id.mainGalleryGrid)
    protected GridView gallery;

    @Bean
    protected GalleryAdapter galleryAdapter;

    @SystemService
    protected LocationManager locationManager;

    private Flickr mFlickr = new Flickr("da4fadd0084ea1799ad33048f0d6a5c5", "186b04791439c326");

    private Location currentLocation;

    protected final String TAG = getClass().getSimpleName();
    private boolean mIsUpdatesEnabled = true;

    @AfterInject
    protected void initLocation() {
        currentLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (currentLocation == null) locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        if (currentLocation == null) Log.d(TAG, "initLocation(): no cached location");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mIsUpdatesEnabled) {
            enableLocationDetection();
            mUpdater.run();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        disableLocationDetection();
        mUpdateHandler.removeCallbacks(mUpdater);
    }

    public void enableLocationDetection() {
        long time = Constants.LOCATION_UPDATE_TIME;
        float dist = Constants.LOCATION_UPDATE_DISTANCE;
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, time, dist, mLocationListener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, time, dist, mLocationListener);
    }

    private void disableLocationDetection() {
        locationManager.removeUpdates(mLocationListener);
    }

    private Handler mUpdateHandler = new Handler();

    @AfterViews
    protected void initUI() {
        gallery.setAdapter(galleryAdapter);
        View emptyView = getLayoutInflater().inflate(R.layout.empty_gallery, null);
        addContentView(emptyView, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        gallery.setEmptyView(emptyView);
    }

    @OptionsItem(R.id.menuRealtimeUpdates)
    protected void onToggleUpdates() {
        mIsUpdatesEnabled = !mIsUpdatesEnabled;
        if (mIsUpdatesEnabled) {
            enableLocationDetection();
            mUpdater.run();
        } else disableLocationDetection();

        supportInvalidateOptionsMenu();
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        menu.findItem(R.id.menuRealtimeUpdates).setChecked(mIsUpdatesEnabled);
        menu.findItem(R.id.menuRefresh).setEnabled(mIsUpdatesEnabled);

        for(int i = 0; i < menu.size(); i++) {
            MenuItem menuItem = menu.getItem(i); // grayout it
            menuItem.getIcon().mutate().setAlpha(menuItem.isEnabled() ? 255 : 64);
        }

        return true;
    }

    @OptionsItem(R.id.menuRefresh)
    protected void onReloadFeed() {
        galleryAdapter.clear();
        updateFeed();
    }

    protected void updateFeed() {
        SearchParameters searchParameters = new SearchParameters();
        if (currentLocation == null) {
            Toast.makeText(this, "Still waiting for location..", Toast.LENGTH_SHORT).show();
            return;
        }
//        attachGPSMock(48.854547, 2.36758, 500);

        searchParameters.setLatitude(String.valueOf(currentLocation.getLatitude()));
        searchParameters.setLongitude(String.valueOf(currentLocation.getLongitude()));
        searchParameters.setMinTakenDate(new Date(System.currentTimeMillis() - 2 * 30 * 24 * 3600 * 1000));
        searchParameters.setMinUploadDate(searchParameters.getMinTakenDate());
        searchParameters.setRadius(5);
        searchParameters.setRadiusUnits("km");
        searchParameters.setAccuracy(1);

        updateFeedRequest(searchParameters, 0);
    }




    @ItemClick(R.id.mainGalleryGrid)
    protected void showFullSize(Photo photo) {
        PhotoFragment photoFragment = PhotoFragment_.builder().photo(photo).build();
        photoFragment.show(getSupportFragmentManager(), "dialog");
    }

    @Background
    protected void updateFeedRequest(SearchParameters searchParameters, int page) {
        try {
            PhotosInterface photosInterface = mFlickr.getPhotosInterface();
            PhotoList photos = photosInterface.search(searchParameters, Constants.PHOTOS_PER_PAGE, page);
            Log.d(TAG, "New feed: " + photos);
            updateFeedShow(photos);
        } catch (Exception e) {
            Log.d(TAG, "updateFeedRequest(): error", e);
        }
    }

    @UiThread
    protected void updateFeedShow(PhotoList photos) {
//        if (photos.getPage() == 0) galleryAdapter.setPhotos(photos);
        galleryAdapter.addNewPhotos(photos);
        gallery.post(new Runnable() {
            @Override
            public void run() {
                gallery.smoothScrollToPosition(0);
            }
        });
    }

    private LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            Log.d(TAG, "onLocationChanged(): " + location);
            currentLocation = location;
            updateFeed();
            rescheduleUpdate();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
        }

        @Override
        public void onProviderEnabled(String provider) {
            if (mIsUpdatesEnabled) {
                locationManager.requestLocationUpdates(provider, Constants.LOCATION_UPDATE_TIME, Constants.LOCATION_UPDATE_DISTANCE, mLocationListener);
            }
        }

        @Override
        public void onProviderDisabled(String provider) {
        }
    };

    private void rescheduleUpdate() {
        mUpdateHandler.removeCallbacks(mUpdater);
        mUpdateHandler.postDelayed(mUpdater, Constants.PHOTOS_UPDATE_TIME);
    }

    private final Runnable mUpdater = new Runnable() {
        @Override
        public void run() {
            Log.d(TAG, "run(): scheduled update start, enabled: " + mIsUpdatesEnabled);

            if (!mIsUpdatesEnabled) return;

            updateFeed();

            rescheduleUpdate();
        }
    };


    private void attachGPSMock(double lat, double lon, int acc) {
        locationManager.addTestProvider(LocationManager.GPS_PROVIDER,
                false, false, false, false, false, false, false,
                Criteria.POWER_LOW, Criteria.ACCURACY_FINE
        );

        Location newLocation = new Location(LocationManager.GPS_PROVIDER);
        newLocation.setLatitude(lat);
        newLocation.setLongitude(lon);
        newLocation.setAccuracy(acc);
        locationManager.setTestProviderEnabled(LocationManager.GPS_PROVIDER, true);
        locationManager.setTestProviderStatus(LocationManager.GPS_PROVIDER,LocationProvider.AVAILABLE,null,System.currentTimeMillis());
        locationManager.setTestProviderLocation(LocationManager.GPS_PROVIDER,newLocation);
    }
}
