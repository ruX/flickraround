package vc.rux.flickraround.ui.components;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import com.googlecode.androidannotations.annotations.AfterInject;
import com.googlecode.androidannotations.annotations.EBean;
import com.googlecode.androidannotations.annotations.RootContext;
import com.googlecode.flickrjandroid.photos.Photo;
import com.googlecode.flickrjandroid.photos.PhotoList;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Produce views for gallery
 */
@EBean
public class GalleryAdapter extends BaseAdapter {
    @RootContext
    protected Context context;

    private PhotoList photos = new PhotoList();
    private ImageLoader mImageLoader = ImageLoader.getInstance();

    @AfterInject
    protected void init() {
        //mImageLoader.init(ImageLoaderConfiguration.createDefault(context));
    }

    @Override
    public int getCount() {
        return photos.size();
    }

    @Override
    public Photo getItem(int position) {
        return photos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return photos.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView view = (convertView == null) ? new RectangleImageView(context) : (ImageView)convertView;
        Photo photo = getItem(position);


        view.setImageDrawable(null);
        view.setScaleType(ImageView.ScaleType.CENTER_CROP);
        view.setLayoutParams(new GridView.LayoutParams(GridView.LayoutParams.MATCH_PARENT, GridView.LayoutParams.WRAP_CONTENT));

        mImageLoader.displayImage(photo.getSmallSquareUrl(), view);

        return view;
    }

    public void setPhotos(PhotoList photos) {
        this.photos = new PhotoList();
        addNewPhotos(photos);
        notifyDataSetInvalidated();
    }

    public PhotoList getPhotos() {
        return photos;
    }

    public void addPhotos(PhotoList photos) {
        this.photos.addAll(photos);
        this.photos.setPages(this.photos.getPages()+1);
        notifyDataSetChanged();
    }

    public void addNewPhotos(PhotoList newPhotos) {
        for (Photo photo: newPhotos) {
            if (photos.contains(photo)) continue;
            photos.add(0, photo);
        }
        notifyDataSetChanged();
    }

    public void clear() {
        photos.clear();
        notifyDataSetInvalidated();
    }

    private static class RectangleImageView extends ImageView {
        public RectangleImageView(Context context) {
            super(context);
        }

        @Override
        public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            super.onMeasure(widthMeasureSpec, widthMeasureSpec);
        }
    }
}
