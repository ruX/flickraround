package vc.rux.flickraround.ui.fragments;

import android.app.DialogFragment;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import com.actionbarsherlock.app.SherlockDialogFragment;
import com.googlecode.androidannotations.annotations.*;
import com.googlecode.flickrjandroid.photos.Photo;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import vc.rux.flickraround.R;

/**
 * Displays full-size photo
 */
@EFragment(R.layout.photo_fragment)
public class PhotoFragment extends SherlockDialogFragment {
    @FragmentArg("photo")
    protected Photo photo;

    @ViewById(R.id.photoFullPhoto)
    protected ImageView photoView;


    @ViewById(R.id.photoTitle)
    protected TextView photoTitle;




    private ImageLoader mImageLoader = ImageLoader.getInstance();

    @AfterInject
    protected void init() {
        mImageLoader.init(ImageLoaderConfiguration.createDefault(getSherlockActivity()));
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, 0);
    }

    @AfterViews
    public void initUI() {
        mImageLoader.displayImage(photo.getLargeUrl(), photoView, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                photoView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }
        });
        photoTitle.setText(photo.getTitle() != null ? photo.getTitle() : "");

        getView().post(new Runnable() {

            @Override
            public void run() {
                if (getDialog() == null) return;
                getDialog().setCanceledOnTouchOutside(true);
            }
        });
    }
}
