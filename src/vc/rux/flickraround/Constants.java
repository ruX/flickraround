package vc.rux.flickraround;

public class Constants {

    public static final int PHOTOS_PER_PAGE = 80;
    public static final long LOCATION_UPDATE_TIME = 15 * 1000;
    public static final long LOCATION_UPDATE_DISTANCE = 50;

    public static final long PHOTOS_UPDATE_TIME = 30 * 1000;
}
