Flickr Around test app
--------------

Sample application for employment. App displays photos taken nearby in realtime.


Build using libraries:

* [AndroidAnnotations](https://github.com/excilys/androidannotations)
* [flickrj-android](http://code.google.com/p/flickrj-android/)
* [slf4j-android](http://www.slf4j.org/android/)
* [ActionBarSherlock](http://actionbarsherlock.com/index.html)
* [Universal Image Loader for Android](https://github.com/nostra13/Android-Universal-Image-Loader)


2013 (c) Ruslan Zaharov aka [ruX](http://ruX.pp.ru/)